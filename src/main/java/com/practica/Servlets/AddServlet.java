package com.practica.Servlets;

import com.practica.DAO.*;
import com.practica.entity.*;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.roaringbitmap.RoaringBitmap;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

/**
 * Created by student on 2/12/2018.
 */
@WebServlet(name = "AddServlet")
public class AddServlet extends HttpServlet {
    private GroupDAO groupDAO = new GroupDAO();
    private PhoneTypeDAO phoneTypeDAO = new PhoneTypeDAO();
    private AddressDAO addressDAO = new AddressDAO();
    private PersonDAO personDAO = new PersonDAO();
    private PhoneDAO phoneDAO = new PhoneDAO();

    @SuppressWarnings("Since15")
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String string = request.getParameter("dateOfBirth");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
        LocalDate date = LocalDate.parse(string, formatter);

        String str = request.getParameter("pictur");
        String dirName="D:\\workspace";
        ByteArrayOutputStream baos = new ByteArrayOutputStream(1000);
        BufferedImage img= ImageIO.read(new File(dirName,str));
        ImageIO.write(img, "png", baos);
        baos.flush();
        byte[] imgByte = baos.toByteArray();
        Address address = new Address();
        address.setCountry(request.getParameter("adr_country"));
        address.setCity(request.getParameter("adr_city"));
        address.setAddress(request.getParameter("adr_address"));
        Person person = new Person();
        person.setFirstName(request.getParameter("firstName"));
        person.setLastName(request.getParameter("lastName"));
        person.setGender(request.getParameter("gender"));
        person.setDateOfBirth(java.sql.Date.valueOf(date));
        StudentDAO studentDAO = new StudentDAO();
        try {
            long idAdr = 0;
            long idPers = 0;
            idAdr =  addressDAO.create(address);
            idPers = personDAO.create(person,idAdr, imgByte);
            studentDAO.add(Long.parseLong(request.getParameter("group")), idPers);
            phoneDAO.create(Long.parseLong(request.getParameter("typePhone")),request.getParameter("valuePhone"),idPers);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        response.sendRedirect("/servlet");



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            request.setAttribute("groups", groupDAO.getAllGroups());
            request.setAttribute("phoneTypes", phoneTypeDAO.getAllPhoneTypes());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        request.getRequestDispatcher("/WEB-INF/add.jsp").forward(request, response);

    }
}
