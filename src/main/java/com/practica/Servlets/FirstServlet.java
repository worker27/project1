package com.practica.Servlets;

import com.practica.DAO.AddressDAO;
import com.practica.DAO.PersonDAO;
import com.practica.DAO.PhoneDAO;
import com.practica.DAO.StudentDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by student on 2/7/2018.
 */
@WebServlet(name = "FirstServlet")
public class FirstServlet extends HttpServlet {
    private PhoneDAO phoneDAO = new PhoneDAO();
    private PersonDAO personDAO = new PersonDAO();
    private StudentDAO studentDAO = new StudentDAO();
  //  private AddressDAO addressDAO = new AddressDAO();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
          //  request.setAttribute("phones", personDAO.getAllPeople());
            request.setAttribute("students", studentDAO.getAllStudents());
        //    request.setAttribute("addresses",phoneDAO.getAllByIdPhone());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
    }
}
