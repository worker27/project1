package com.practica.Servlets;

import com.practica.DAO.GroupDAO;
import com.practica.DAO.PhoneDAO;
import com.practica.DAO.PhoneTypeDAO;
import com.practica.DAO.StudentDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by student on 2/15/2018.
 */
@WebServlet(name = "EditServlet")
public class EditServlet extends HttpServlet {
    StudentDAO studentDAO = new StudentDAO();
    PhoneDAO phoneDAO = new PhoneDAO();
    PhoneTypeDAO phoneTypeDAO = new PhoneTypeDAO();
    GroupDAO groupDAO = new GroupDAO();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long id = Long.parseLong(request.getParameter("id"));

        try {
            request.setAttribute("student", studentDAO.getByIdStudent(id));
            request.setAttribute("phone", phoneDAO.getAllByIdPhone(id));
            request.setAttribute("phoneTypes", phoneTypeDAO.getAllPhoneTypes());
            request.setAttribute("groups", groupDAO.getAllGroups());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("/WEB-INF/edit.jsp").forward(request, response);
    }

}
