package com.practica.entity;

import java.util.Date;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class Mark {
    private Long id;
    private Student student;
    private List<Discipline> discipline;
    private Teacher teacher;
    private Double value;
    private Date createDate;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public Student getStudent(){
        return student;
    }

    public void setStudent(Student student){
        this.student = student;
    }

    public List<Discipline> getDiscipline(){
        return discipline;
    }

    public void setDiscipline(List<Discipline> discipline){
        this.discipline = discipline;
    }

    public Teacher getTeacher(){
        return teacher;
    }

    public void setTeacher(Teacher teacher){
        this.teacher = teacher;
    }

    public Double getValue(){
        return value;
    }

    public void setValue(Double value){
        this.value = value;
    }

    public Date getCreateDate(){
        return createDate;
    }

    public void setCreateDate(Date createDate){
        this.createDate = createDate;
    }


}
