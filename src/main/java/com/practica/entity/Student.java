package com.practica.entity;

import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class Student extends Person{
    private Group group;
    private List<Discipline> discipline;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<Discipline> getDiscipline() {
        return discipline;
    }

    public void setDiscipline(List<Discipline> discipline) {
        this.discipline = discipline;
    }
}
