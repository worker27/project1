package com.practica.entity;

import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class Discipline {
    private Long id;
    private String title;
    private Teacher teacher;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public Teacher getTeacher(){
        return teacher;
    }

    public void setTeacher(Teacher teacher){
        this.teacher = teacher;
    }
}
