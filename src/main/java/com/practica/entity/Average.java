package com.practica.entity;

import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class Average {
    private Long id;
    private Student student;
    private Double value;
    private List<Discipline> discipline;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public Student getStudent(){
        return student;
    }

    public void setStudent(Student student){
        this.student = student;
    }

    public Double getValue(){
        return value;
    }

    public void setValue(Double value){
        this.value = value;
    }

    public List<Discipline> getDiscipline(){
        return discipline;
    }

    public void setDiscipline(List<Discipline> discipline){
        this.discipline = discipline;
    }


}
