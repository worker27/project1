package com.practica.entity;

/**
 * Created by student on 2/6/2018.
 */
public class Teacher extends Person{
    private Double salary;

    public Double getSalary(){
        return salary;
    }

    public void setSalary(Double salary){
        this.salary = salary;
    }

}
