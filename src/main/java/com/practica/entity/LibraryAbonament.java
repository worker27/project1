package com.practica.entity;

import java.util.Date;

/**
 * Created by student on 2/6/2018.
 */
public class LibraryAbonament {
    private Long id;
    private Date startDate;
    private Date endDate;
    private String status;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public Date getStartDate(){
        return startDate;
    }

    public void setStartDate(Date startDate){
        this.startDate = startDate;
    }

    public Date getEndDate(){
        return endDate;
    }

    public void setEndDate(Date endDate){
        this.endDate = endDate;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }
}
