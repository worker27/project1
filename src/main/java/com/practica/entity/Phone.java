package com.practica.entity;

/**
 * Created by student on 2/6/2018.
 */
public class Phone {
    private Long id;
    private PhoneType phoneType;
    private String value;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public PhoneType getPhoneType(){
        return phoneType;
    }

    public void setPhoneType(PhoneType phoneType){
        this.phoneType = phoneType;
    }

    public String getValue(){
        return value;
    }

    public void setValue(String value){
        this.value = value;
    }
}
