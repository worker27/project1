package com.practica.entity;

/**
 * Created by student on 2/6/2018.
 */
public class PhoneType {
    private Long id;
    private String name;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
}
