package com.practica.DAO;

import com.practica.Connections.Settings;
import com.practica.entity.Mark;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/8/2018.
 */
public class MarkDAO {
    private PreparedStatement preparedStatement = null;
    private DisciplineDAO disciplineDAO = new DisciplineDAO();
    private TeacherDAO teacherDAO = new TeacherDAO();
    private StudentDAO studentDAO = new StudentDAO();
    public MarkDAO() {

    }

    public Mark getByIdMark(Long id) throws SQLException, ClassNotFoundException, IOException {
        Mark mark = new Mark();
        preparedStatement = Settings.getConnection().prepareStatement("SELECT * FROM marks WHERE id = ?");
        preparedStatement.setLong(1,id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        mark.setId(rs.getLong("id"));
        mark.setValue(rs.getDouble("value"));
        mark.setCreateDate(rs.getDate("create_data"));
        mark.setDiscipline(disciplineDAO.getAllByIdDiscipline(rs.getLong("discipline_id")));
        mark.setStudent(studentDAO.getByIdStudent(rs.getLong("student_id")));
        mark.setTeacher(teacherDAO.getByIdTeacher(rs.getLong("teacher_id")));
        return mark;
    }

    public List<Mark> getAllMarks() throws SQLException, ClassNotFoundException, IOException {
        List<Mark> marks = new ArrayList<Mark>();
        preparedStatement = Settings.getConnection().prepareStatement("SELECT * FROM marks");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()){
            Mark mark = new Mark();
            mark.setId(rs.getLong("id"));
            mark.setValue(rs.getDouble("value"));
            mark.setCreateDate(rs.getDate("create_data"));
            mark.setDiscipline(disciplineDAO.getAllByIdDiscipline(rs.getLong("discipline_id")));
            mark.setStudent(studentDAO.getByIdStudent(rs.getLong("student_id")));
            mark.setTeacher(teacherDAO.getByIdTeacher(rs.getLong("teacher_id")));
            marks.add(mark);
        }
        return marks;
    }

    public void addMark(Mark mark) throws SQLException, ClassNotFoundException {
        preparedStatement = Settings.getConnection().prepareStatement("INSERT INTO marks (student_id, discipline_id, teacher_id, value, create_data) VALUES (?, ?, ?, ?, ?)", preparedStatement.RETURN_GENERATED_KEYS);
    }
}
