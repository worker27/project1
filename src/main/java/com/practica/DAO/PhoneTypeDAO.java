package com.practica.DAO;

import com.practica.Connections.Settings;
import com.practica.entity.PhoneType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class PhoneTypeDAO {
    private PreparedStatement preparedStatement2 = null;

    public PhoneTypeDAO() {

    }

    public PhoneType getByIdPhoneType(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement2 = Settings.getConnection().prepareStatement("Select * from phone_types where id=?");
        preparedStatement2.setLong(1,id);
        ResultSet rs = preparedStatement2.executeQuery();
        PhoneType phoneType = new PhoneType();
        rs.next();
        phoneType.setId(rs.getLong("id"));
        phoneType.setName(rs.getString("name"));
        return phoneType;
    }

    public List<PhoneType> getAllPhoneTypes() throws SQLException, ClassNotFoundException {
        ArrayList<PhoneType> phoneTypes = null;
        preparedStatement2 = Settings.getConnection().prepareStatement("Select * from phone_types");
        ResultSet rs = preparedStatement2.executeQuery();
        phoneTypes = new ArrayList<PhoneType>(rs.getFetchSize());
        while (rs.next()) {
            PhoneType pt = new PhoneType();
            pt.setId(rs.getLong("id"));
            pt.setName(rs.getString("name"));
            phoneTypes.add(pt);
        }
        return phoneTypes;
    }
}
