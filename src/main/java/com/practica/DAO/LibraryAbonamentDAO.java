package com.practica.DAO;

import com.practica.Connections.Settings;
import com.practica.entity.LibraryAbonament;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class LibraryAbonamentDAO {
    private PreparedStatement preparedStatement = null;

    public LibraryAbonamentDAO() {

    }

    public LibraryAbonament getByIdLibraryAbonament(Long id) throws SQLException, ClassNotFoundException {
            preparedStatement = Settings.getConnection().prepareStatement("Select * from library_abonaments where id=?");
            preparedStatement.setLong(1,id);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            LibraryAbonament libraryAbonament = new LibraryAbonament();
            libraryAbonament.setId(rs.getLong("id"));
            libraryAbonament.setStartDate(rs.getDate("start_date"));
            libraryAbonament.setEndDate(rs.getDate("end_date"));
            libraryAbonament.setStatus(rs.getString("status"));
            return libraryAbonament;

    }

    public List<LibraryAbonament> getAllLibraryAbonament() throws SQLException, ClassNotFoundException {
        ArrayList<LibraryAbonament> libraryAb = null;
        preparedStatement = Settings.getConnection().prepareStatement("SELECT * from library_abonaments");
        ResultSet rs = preparedStatement.executeQuery();
        libraryAb = new ArrayList<LibraryAbonament>(rs.getFetchSize());
        while (rs.next()) {
            LibraryAbonament libAb = new LibraryAbonament();
            libAb.setId(rs.getLong("id"));
            libAb.setStartDate(rs.getDate("start_date"));
            libAb.setEndDate(rs.getDate("end_date"));
            libAb.setStatus(rs.getString("status"));
            libraryAb.add(libAb);
        }
        return libraryAb;
    }
}
