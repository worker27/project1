package com.practica.DAO;

import com.practica.Connections.Settings;
import com.practica.entity.Address;
import com.practica.entity.LibraryAbonament;
import com.practica.entity.Person;
import com.practica.entity.Phone;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/7/2018.
 */
public class PersonDAO {
    private PreparedStatement preparedStatement =null;
    private AddressDAO addressDAO = new AddressDAO();
    private LibraryAbonamentDAO libraryAbonamentDAO = new LibraryAbonamentDAO();
    private PhoneDAO phoneDAO = new PhoneDAO();

    public PersonDAO() {

    }

    public List<Person> getAllPeople() throws SQLException, ClassNotFoundException, IOException {
        List<Person> people = new ArrayList<Person>();
        preparedStatement = Settings.getConnection().prepareStatement("Select * from persons ");
        ResultSet rs = preparedStatement.executeQuery();

        while (rs.next()){
            Person person = new Person();
            byte[] byteImg = rs.getBytes("picture");
            person.setId(rs.getLong("id"));
            person.setFirstName(rs.getString("first_name"));
            person.setLastName(rs.getString("last_name"));
            person.setDateOfBirth(rs.getDate("date_of_birth"));
            person.setGender(rs.getString("gender"));
            person.setPicture(Base64.encode(byteImg));
            person.setMail(rs.getString("mail"));
            person.setAddress(addressDAO.getByIdAddress(rs.getLong("address_id")));
            person.setLibraryAbonament(libraryAbonamentDAO.getByIdLibraryAbonament(rs.getLong("library_abonament_id")));
            person.setPhones((List<Phone>) phoneDAO.getAllByIdPhone(rs.getLong("id")));
            people.add(person);

        }
        return people;
    }

    public long create(Person person, long id_address, byte[] imgByte) throws SQLException,ClassNotFoundException{
        long keyId = 0;
        preparedStatement = Settings.getConnection().prepareStatement("INSERT INTO persons (first_name, last_name, gender, address_id, library_abonament_id, date_of_birth, picture) VALUES (?, ?, ?, ?, 3, ?, ?)", preparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, person.getFirstName());
        preparedStatement.setString(2, person.getLastName());
        preparedStatement.setString(3, person.getGender());
        preparedStatement.setLong(4, id_address);
        preparedStatement.setDate(5, person.getDateOfBirth());
        preparedStatement.setBytes(6, imgByte);
        preparedStatement.executeUpdate();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        if (rs.next()) {
            keyId = rs.getLong(9);
        }
        return keyId;
    }


}


    /*File fnew=new File("/tmp/rose.jpg");
    BufferedImage originalImage=ImageIO.read(fnew);
    ByteArrayOutputStream baos=new ByteArrayOutputStream();
ImageIO.write(originalImage, "jpg", baos );
        byte[] imageInByte=baos.toByteArray();*/