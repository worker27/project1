package com.practica.DAO;

import com.practica.Connections.Settings;
import com.practica.entity.Group;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class GroupDAO {
    private PreparedStatement preparedStatement = null;

    public GroupDAO() {

    }

    public Group getByIdGroup(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Settings.getConnection().prepareStatement("Select * from groups where id=?");
        preparedStatement.setLong(1,id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        Group group = new Group();
        group.setId(rs.getLong("id"));
        group.setName(rs.getString("name"));
        return group;
    }

    public List<Group> getAllGroups() throws SQLException, ClassNotFoundException {
        List<Group> groups = new ArrayList<Group>();
        preparedStatement = Settings.getConnection().prepareStatement("SELECT * from groups");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Group gr = new Group();
            gr.setId(rs.getLong("id"));
            gr.setName(rs.getString("name"));
            groups.add(gr);
        }
        return groups;
    }

    public void deleteById(Group group) throws SQLException, ClassNotFoundException{
        preparedStatement = Settings.getConnection().prepareStatement("DELETE FROM groups WHERE id = ?");
        preparedStatement.setLong(1, group.getId());
        preparedStatement.executeUpdate();
    }

    public void add(String name) throws SQLException, ClassNotFoundException{
        try {
            preparedStatement = Settings.getConnection().prepareStatement("INSERT INTO groups(name) VALUES(?)");
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();



    } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

