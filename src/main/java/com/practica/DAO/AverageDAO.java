package com.practica.DAO;

import com.practica.Connections.Settings;
import com.practica.entity.Average;
import com.practica.entity.Discipline;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/8/2018.
 */
public class AverageDAO {
    private PreparedStatement preparedStatement = null;
    private StudentDAO studentDAO = new StudentDAO();
    private DisciplineDAO disciplineDAO = new DisciplineDAO();
    public AverageDAO() {

    }

    public Average getByIdAverage(Long id) throws SQLException, ClassNotFoundException, IOException {
        Average average = new Average();
        preparedStatement = Settings.getConnection().prepareStatement("SELECT * FROM averages WHERE id = ?");
        preparedStatement.setLong(1,id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        average.setId(rs.getLong("id"));
        average.setDiscipline(disciplineDAO.getAllByIdDiscipline(rs.getLong("discipline_id")));
        average.setStudent(studentDAO.getByIdStudent(rs.getLong("student_id")));
        average.setValue(rs.getDouble("value"));
        return average;
    }

    public List<Average> getAllAverages() throws SQLException, ClassNotFoundException, IOException {
        List<Average> averages = new ArrayList<Average>();
        preparedStatement = Settings.getConnection().prepareStatement("SELECT * FROM averages");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Average average = new Average();
            average.setId(rs.getLong("id"));
            average.setDiscipline(disciplineDAO.getAllByIdDiscipline(rs.getLong("discipline_id")));
            average.setStudent(studentDAO.getByIdStudent(rs.getLong("student_id")));
            average.setValue(rs.getDouble("value"));
            averages.add(average);
        }
        return averages;
    }

    public void deleteById(Average average) throws SQLException, ClassNotFoundException {
        preparedStatement = Settings.getConnection().prepareStatement("DELETE FROM averages WHERE id = ?");
        preparedStatement.setLong(1, average.getId());
        preparedStatement.executeUpdate();
    }

    public Average save() throws SQLException, ClassNotFoundException {
        Average average = new Average();
        average.setId((long) 2);
        average.setValue(213.32);
      //  average.setStudent();
        return average;
    }
}
