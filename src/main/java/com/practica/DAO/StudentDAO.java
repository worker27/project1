package com.practica.DAO;

import com.practica.Connections.Settings;
import com.practica.entity.Discipline;
import com.practica.entity.Group;
import com.practica.entity.Student;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import javax.imageio.ImageIO;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/7/2018.
 */
public class StudentDAO {
    private PreparedStatement preparedStatement = null;
    private GroupDAO groupDAO = new GroupDAO();
    private DisciplineDAO disciplineDAO = new DisciplineDAO();
    private AddressDAO addressDAO = new AddressDAO();
    private LibraryAbonamentDAO libraryAbonamentDAO = new LibraryAbonamentDAO();
    private PhoneDAO phoneDAO = new PhoneDAO();

    public StudentDAO() {

    }

    public Student getByIdStudent(Long id) throws SQLException, ClassNotFoundException, IOException {
        Student student = new Student();
        preparedStatement = Settings.getConnection().prepareStatement("SELECT * FROM students JOIN persons ON students.id = persons.id WHERE students.id = ?" );
        preparedStatement.setLong(1,id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        byte[] byteImg = rs.getBytes("picture");
        student.setId(rs.getLong("id"));
        student.setGroup(groupDAO.getByIdGroup(rs.getLong("group_id")));
     //   student.setDiscipline(disciplineDAO.getAllByIdDiscipline(rs.getLong("discipline_id")));
        student.setFirstName(rs.getString("first_name"));
        student.setLastName(rs.getString("last_name"));
        student.setDateOfBirth(rs.getDate("date_of_birth"));
        student.setGender(rs.getString("gender"));
        student.setAddress(addressDAO.getByIdAddress(rs.getLong("address_id")));
        student.setLibraryAbonament(libraryAbonamentDAO.getByIdLibraryAbonament(rs.getLong("library_abonament_id")));
        student.setMail(rs.getString("mail"));
        student.setPicture(Base64.encode(byteImg));
        student.setPhones( phoneDAO.getAllByIdPhone(rs.getLong("id")));
        return student;
    }

    public List<Student> getAllStudents() throws SQLException, ClassNotFoundException, IOException {
        List<Student> students = new ArrayList<Student>();
        preparedStatement = Settings.getConnection().prepareStatement("Select * from students join persons on students.id = persons.id");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Student student = new Student();
            byte[] byteImg = rs.getBytes("picture");
            student.setId(rs.getLong("id"));
            student.setGroup(groupDAO.getByIdGroup(rs.getLong("group_id")));
            student.setDiscipline((List<Discipline>) disciplineDAO.getAllByIdDiscipline(rs.getLong("id")));
            student.setFirstName(rs.getString("first_name"));
            student.setLastName(rs.getString("last_name"));
            student.setDateOfBirth(rs.getDate("date_of_birth"));
            student.setGender(rs.getString("gender"));
            student.setAddress(addressDAO.getByIdAddress(rs.getLong("address_id")));
            student.setLibraryAbonament(libraryAbonamentDAO.getByIdLibraryAbonament(rs.getLong("library_abonament_id")));
            student.setMail(rs.getString("mail"));
            student.setPicture(Base64.encode(byteImg));
            student.setPhones( phoneDAO.getAllByIdPhone(rs.getLong("id")));
            students.add(student);
        }
        return students;
    }

    public void add(long id_group, long id_pers) throws SQLException, ClassNotFoundException {
        preparedStatement = Settings.getConnection().prepareStatement("INSERT INTO students (group_id, id) VALUES (?, ?)");
        preparedStatement.setLong(1,id_group);
        preparedStatement.setLong(2,id_pers);
        preparedStatement.executeUpdate();
    }

   /* public void addStudent(Student Student) {
        Student student = new Student();
        byte[] byteImg = rs.getBytes("picture");
        student.setId(rs.getLong("id"));
        student.setGroup(groupDAO.getByIdGroup(rs.getLong("group_id")));
        student.setDiscipline((List<Discipline>) disciplineDAO.getAllByIdDiscipline(rs.getLong("id")));
        student.setFirstName(rs.getString("first_name"));
        student.setLastName(rs.getString("last_name"));
        student.setDateOfBirth(rs.getDate("date_of_birth"));
        student.setGender(rs.getString("gender"));
        student.setAddress(addressDAO.getByIdAddress(rs.getLong("address_id")));
        student.setLibraryAbonament(libraryAbonamentDAO.getByIdLibraryAbonament(rs.getLong("library_abonament_id")));
        student.setMail(rs.getString("mail"));
        student.setPicture(Base64.encode(byteImg));
        student.setPhones( phoneDAO.getAllByIdPhone(rs.getLong("id")));

    }*/
}
