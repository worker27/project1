package com.practica.DAO;

import com.practica.Connections.Settings;
import com.practica.entity.Discipline;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/8/2018.
 */
public class DisciplineDAO {
    private PreparedStatement preparedStatement = null;
    private TeacherDAO teacherDAO = new TeacherDAO();


    public DisciplineDAO() {

    }


    public List<Discipline> getAllByIdDiscipline(Long id) throws SQLException, ClassNotFoundException {
        List<Discipline> disciplines = new ArrayList<Discipline>();
        preparedStatement = Settings.getConnection().prepareStatement("SELECT * FROM disciplines JOIN disciplines_to_students on disciplines.id = disciplines_to_students.discipline_id WHERE student_id = ?");
        preparedStatement.setLong(1,id);
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Discipline discipline = new Discipline();
            discipline.setId(rs.getLong("id"));
            discipline.setTitle(rs.getString("title"));
            discipline.setTeacher(teacherDAO.getByIdTeacher(rs.getLong("teacher_id")));
            disciplines.add(discipline);
        }
    return disciplines;
    }

    public List<Discipline> getAllDisciplines() throws SQLException, ClassNotFoundException {
        List<Discipline> disciplines = new ArrayList<Discipline>();
        preparedStatement = Settings.getConnection().prepareStatement("SELECT * FROM disciplines");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Discipline discipline = new Discipline();
            discipline.setId(rs.getLong("id"));
            discipline.setTitle(rs.getString("title"));
            discipline.setTeacher(teacherDAO.getByIdTeacher(rs.getLong("theacher_id")));
            disciplines.add(discipline);
        }
        return disciplines;
    }
}
