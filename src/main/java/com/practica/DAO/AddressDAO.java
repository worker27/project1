package com.practica.DAO;

import com.practica.Connections.Settings;
import com.practica.entity.Address;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class AddressDAO {
    private PreparedStatement preparedStatement = null;

    public AddressDAO() {

    }

    public Address getByIdAddress(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Settings.getConnection().prepareStatement("Select * from addresses where id=?");
        preparedStatement.setLong(1,id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        Address address = new Address();
        address.setId(rs.getLong("id"));
        address.setCountry(rs.getString("country"));
        address.setAddress(rs.getString("address"));
        address.setCity(rs.getString("city"));
        return address;
    }

    public List<Address> getAllAddresses() throws SQLException, ClassNotFoundException {
        List<Address> addresses = new ArrayList<Address>();
        preparedStatement = Settings.getConnection().prepareStatement("Select * from addresses");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Address adr = new Address();
            adr.setCity(rs.getString("city"));
            adr.setCountry(rs.getString("country"));
            adr.setAddress(rs.getString("address"));
            adr.setId(rs.getLong("id"));
            addresses.add(adr);
        }
        return addresses;
    }

    public long create(Address address) throws SQLException, ClassNotFoundException {
        long keyId = 0;
        preparedStatement = Settings.getConnection().prepareStatement("INSERT INTO addresses (country, city, address) VALUES (?, ?, ?)", preparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, address.getCountry());
        preparedStatement.setString(2, address.getCity());
        preparedStatement.setString(3, address.getAddress());
        preparedStatement.executeUpdate();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        if (rs.next()) {
            keyId = rs.getLong(1);
        }
        return keyId;
    }
}
