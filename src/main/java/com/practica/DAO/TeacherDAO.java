package com.practica.DAO;

import com.practica.Connections.Settings;
import com.practica.entity.Person;
import com.practica.entity.Teacher;
import com.sun.org.apache.regexp.internal.RE;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/8/2018.
 */
public class TeacherDAO {
    private PreparedStatement preparedStatement = null;
    private AddressDAO addressDAO = new AddressDAO();
    private LibraryAbonamentDAO libraryAbonamentDAO = new LibraryAbonamentDAO();

    public TeacherDAO() {

    }

    public Teacher getByIdTeacher(Long id) throws SQLException, ClassNotFoundException {
        Teacher teacher = new Teacher();
        preparedStatement = Settings.getConnection().prepareStatement("SELECT * FROM  teachers JOIN persons ON teachers.id = persons.id WHERE teachers.id = ?");
        preparedStatement.setLong(1,id);
        ResultSet rs = preparedStatement.executeQuery();
        rs.next();
        teacher.setId(rs.getLong("id"));
        teacher.setSalary(rs.getDouble("salary"));
        teacher.setFirstName(rs.getString("first_name"));
        teacher.setLastName(rs.getString("last_name"));
        teacher.setDateOfBirth(rs.getDate("date_of_birth"));
        teacher.setGender(rs.getString("gender"));
        teacher.setMail(rs.getString("mail"));
        //teacher.setPicture(rs.getBytes("picture"));
        teacher.setLibraryAbonament(libraryAbonamentDAO.getByIdLibraryAbonament(rs.getLong("library_abonament_id")));
        teacher.setAddress(addressDAO.getByIdAddress(rs.getLong("address_id")));
        return teacher;
    }

    public List<Teacher> getAllTeacher() throws SQLException, ClassNotFoundException {
        List<Teacher> teachers = new ArrayList<Teacher>();
        preparedStatement = Settings.getConnection().prepareStatement("SELECT * FROM teachers JOIN persons ON teachers.id = persons.id");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Teacher teacher = new Teacher();
            teacher.setId(rs.getLong("id"));
            teacher.setSalary(rs.getDouble("salary"));
            teacher.setFirstName(rs.getString("first_name"));
            teacher.setLastName(rs.getString("last_name"));
            teacher.setDateOfBirth(rs.getDate("date_of_birth"));
            teacher.setGender(rs.getString("gender"));
            teacher.setMail(rs.getString("mail"));
            //teacher.setPicture(rs.getBytes("picture"));
            teacher.setLibraryAbonament(libraryAbonamentDAO.getByIdLibraryAbonament(rs.getLong("library_abonament_id")));
            teacher.setAddress(addressDAO.getByIdAddress(rs.getLong("address_id")));
            teachers.add(teacher);
        }
        return teachers;
    }
}
