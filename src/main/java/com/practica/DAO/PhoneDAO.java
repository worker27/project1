package com.practica.DAO;

import com.practica.Connections.Settings;
import com.practica.entity.Phone;
import com.practica.entity.PhoneType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by student on 2/6/2018.
 */
public class PhoneDAO {
    private PreparedStatement preparedStatement = null;
    private PhoneTypeDAO phoneTypeDAO = new PhoneTypeDAO();

    public PhoneDAO() {

    }

    public List<Phone> getAllByIdPhone(Long id) throws SQLException, ClassNotFoundException {
        preparedStatement = Settings.getConnection().prepareStatement("Select * from phones  JOIN persons_to_phones ON id = persons_to_phones.phone_id where person_id = ?");
        preparedStatement.setLong(1,id);
        List<Phone> phones = new ArrayList<Phone>();
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Phone phone = new Phone();
            phone.setId(rs.getLong("id"));
            phone.setValue(rs.getString("value"));
            phone.setPhoneType(phoneTypeDAO.getByIdPhoneType(rs.getLong("type_id")));
            phones.add(phone);
        }
        return phones;
    }

    public List<Phone> getAllPhones() throws SQLException, ClassNotFoundException {
        List<Phone> phones = new ArrayList<Phone>();
        preparedStatement = Settings.getConnection().prepareStatement("select * from phones ");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            Phone phone = new Phone();
            phone.setId(rs.getLong("id"));
            phone.setValue(rs.getString("value"));
            phone.setPhoneType(phoneTypeDAO.getByIdPhoneType(rs.getLong("type_id")));
            phones.add(phone);
        }
        return phones;
    }

    public void create(long id_type, String value, long idPers) throws SQLException, ClassNotFoundException {
        long keyId = 0;
        preparedStatement = Settings.getConnection().prepareStatement("INSERT INTO phones (type_id, value) VALUES (?, ?)", preparedStatement.RETURN_GENERATED_KEYS);
        preparedStatement.setLong(1, id_type);
        preparedStatement.setLong(2, Long.parseLong(value));
        preparedStatement.executeUpdate();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        if (rs.next()){
            keyId = rs.getLong(3);
       //     preparedStatement.getConnection().close();
        }
        preparedStatement = Settings.getConnection().prepareStatement("INSERT INTO persons_to_phones (person_id, phone_id) VALUES (?, ?)");
        preparedStatement.setLong(1, idPers);
        preparedStatement.setLong(2, keyId);
        preparedStatement.executeUpdate();

    }

}
