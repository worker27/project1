package com.practica.Connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by student on 2/6/2018.
 */
public class Settings {

    private static Connection connection;
    private static org.postgresql.Driver driver1;

    public Settings () {

    }

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        driver1 = new org.postgresql.Driver();
        Class.forName(driver1.getClass().getName());
        if (connection == null) {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Practica", "postgres", "postgres");

        }
        return connection;
    }

    public static void resetAll() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
