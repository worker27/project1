<%@ page import="javax.imageio.ImageIO" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <link href="style.css" rel="stylesheet"  type="text/css">
    <title>$Title$</title>

  </head>
  <body>
  <div class="container">
    <h2>List Students</h2>
    <hr>
    <table border="3">
      <tr>
        <th>Picture</th>
        <th>Name</th>
        <th>birthday</th>
        <th>Address</th>
        <th>Phones</th>
        <th>Disciplines</th>
      </tr>
      <c:forEach var="student" items="${students}">
      <tr>
        <td><img src="data:image/png;base64,${student.picture}" alt=""></td>
        <td><c:out value="${student.firstName}"/> <c:out value="${student.lastName}"/></td>
        <td><c:out value="${student.dateOfBirth}"/></td>

        <td><c:out value="${student.address.city}"/> <c:out value="${student.address.address}"/></td>
        <td><c:forEach var="phone" items="${student.phones}">
          <c:out value="${phone.phoneType.name}"/>:<c:out value="${phone.value}"/> <br>
        </c:forEach>
        </td>
        <td><c:forEach var="disciplin" items="${student.discipline}">
          <c:out value="${disciplin.title}"/> <br>
        </c:forEach>
        </td>
        <td>
          <button name="btnEdit" value="<c:out value="${student.id}"/>">Edit student</button>
          <a href="${pageContext.request.contextPath}/edit?id=${student.id}" class="btn-student">wdgdsfgfdg</a>
        </td>
      </tr>
      </c:forEach>
    </table>
  </div>









  </body>
</html>
