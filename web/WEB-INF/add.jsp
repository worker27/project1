<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student
  Date: 2/12/2018
  Time: 4:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/style.css" media="screen"/>
    <link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
    <title>Add student</title>
</head>
<body>
    <form action="${pageContext.request.contextPath}/add" method="post">
        <label for="firstName">First Name: </label>
        <input type="text" id="firstName" name="firstName"><br>
        <label for="lastName">Last Name: </label>
        <input type="text" id="lastName" name="lastName"><br>
        <label for="dateOfBirth">Date of birth: </label>
        <input type="date" id="dateOfBirth" name="dateOfBirth"><br>
        <label>Gender: </label>
        <input type="radio" name="gender" value="m" id="male">
        <label for="male">Male</label>
        <input type="radio" name="gender" value="f" id="female">
        <label for="female">Female</label><br>
        <label for="adr_country">Country</label>
        <input type="text" id="adr_country" name="adr_country"><br>
        <label for="adr_city">City</label>
        <input type="text" id="adr_city" name="adr_city"><br>
        <label for="adr_address">Address</label>
        <input type="text" id="adr_address" name="adr_address"><br>
        <label>Phone(s): </label>
        <select name="typePhone">
            <c:forEach var="phoneType" items="${phoneTypes}">
            <option value="<c:out value="${phoneType.id}"/>"><c:out value="${phoneType.name}"/></option>
            </c:forEach>
        </select>
        <input type="text" name="valuePhone"><br>
        <label>Group </label>
        <select name="group" id="groupId">
            <c:forEach var="group" items="${groups}">
            <option value="<c:out value="${group.id}"/>"><c:out value="${group.name}"/></option>
            </c:forEach>
        </select><br>
        <img id="blah" src="#" alt="your image" />
        <input type="file" id="pict" name="pictur" onchange="readURL(this);"><br>
        <input type="submit">
    </form>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(200)
                        .height(178);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
</body>
</html>
